package com.desarrolloandroid.rodrigoleiva;


import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.app.Activity;
import android.content.pm.ActivityInfo;

public class Aplicacion extends Activity implements OnBufferingUpdateListener, OnClickListener{
	
	private ImageButton botonPlay;
	private ImageButton botonPause;
	private ImageButton botonStop;
	
	MediaPlayer radio;
	
	
		
	//Creación de la vista xml y posterior establecimiento de la vista para el programa java".
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capa_layout);
        
        //Dejar fija la aplicación en vertical, evitando que se gire y se desordene en modo horizontal. 
        
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        inicializa();
        
        
    }


    	//inicializar botones y asignarlos mediante el "id" del archivo "capa_layout.xml"
    
	private void inicializa() {
		// TODO Auto-generated method stub
	
		botonPlay = (ImageButton)findViewById(R.id.Boton_Repr1);
		botonPause = (ImageButton)findViewById(R.id.Boton_Repr2);
		botonStop = (ImageButton)findViewById(R.id.Boton_Repr3);
		
		botonPlay.setOnClickListener(this);
		botonPause.setOnClickListener(this);
		botonStop.setOnClickListener(this);
			
		
		radio = new MediaPlayer();
		radio.setOnBufferingUpdateListener(this);
		
	
	}


	//Buffering de transmisión de audio 
	
	public void onBufferingUpdate(MediaPlayer radio, int porcentaje) {
		// TODO Auto-generated method stub
		
	}

	//Método onDestroy(), me permite cerrar la activity (según diagrama de actividad android)
	//Método anulado (12 de Diciembre) debido a que compilador envia error de no poder hacer 
	//onDestroy(), comentado las siguientes lineas permite que la aplicación
	//cierre de buena manera, pero no se sigue el curso normal de la vida
	//de la activity según esta diagramado en Google SDK.
	
//	public void onDestroy() {
//		
//		if(radio != null){
//			
//			radio.stop();
//			radio.release();
//			
//			try {
//				onFinish();
//			} catch (IllegalArgumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IllegalStateException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//				
//		}
//						
//	}
	
	//Método el cual chequea si se presionó el botón "back",
	//en caso efectivo, envía mensaje de salida y cierra activity, 
	//poniendo en estado OFF la reproducción con anterioridad.
	
		
	public void onFinish() throws IOException, IllegalArgumentException, IllegalStateException {
		
		Handler handler;
		
		handler = new Handler();
		
		handler.post(new Runnable() {
			
			public void run() {
			
			finish();
			
			}
			
						
		});
			
		
	}
	
	//Método pausa del Diagrama de Activities, en caso de llamadas
	//mensajes, etc., el reproductor deja en estado "pause" el 
	//objeto "radio" correspondiente al reproductor.
	

	public boolean onKeyDown(int keyCode, KeyEvent  event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
        
        String tag = null;
        
		Log.d(tag,"Sales de la aplicación");
        
		radio.stop();
		radio.release();
		       
        try {
			onFinish();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		        
     }
        return true;
}
	
	
	//Instancia onClick(Objeto de tipo "vista") en la cual se comprueba qué botón se ha pulsado
	//Si play, pause o en su defecto, stop.

	public void onClick(View v) throws IllegalArgumentException, IllegalStateException{
		// TODO Auto-generated method stub
		String url = "http://audio.uctradio.cl/radio";
		
		//if, si se pulsó "play"
		
		if(v.getId() == R.id.Boton_Repr1){
			
			try {
				radio.setAudioStreamType(AudioManager.STREAM_MUSIC);
				radio.setDataSource(url);
				radio.prepareAsync();
				
			//cabe recordar que los "Exception" son arrojados automáticamente por el IDE, a través
			//de "try y catch" como sugerencia
			//a cuando se establece "setDataSource".
				
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//comprobamos si se está reproduciendo, sino se reproduce y está en proceso "reproducir",
			//"obligar" a android a comenzar con el streaming
			
			if(!radio.isPlaying()){
				
				radio.start();
											
			}
					
		} else {
			
			//verificamos los casos de botón "pause" y "stop"
			
				if(v.getId() == R.id.Boton_Repr2){
			
					radio.pause();
			
			
				} else {
			
					
					//para el caso de "stop", dejar en estado de inicio al objeto MediaPlayer y cerrar la activity
			
					if(v.getId() == R.id.Boton_Repr3){
			
						radio.stop();
						radio.release();
						
				        try {
							onFinish();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
				        
					}
			
			
			
				}
			
			
		}
		
				
	}   
        
 
}    
